package sample01;

public class HelloSpring {
	public static void main(String[] args) {
		// 팩토리를 이용해서 타입에 따라 참조 클래스 변경
		MessageBean bean = MessageBeanFactory.getInstance("ko");
		bean.sayHello("spring");
	}
}
