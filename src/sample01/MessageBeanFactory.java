package sample01;

public class MessageBeanFactory {
	public static MessageBean getInstance(String type) {
		switch (type) {
		case "en": {
			return new MessageBeanEnglish();
		}
		case "ko": {
			return new MessageBeanKorean();
		}
		default: {
			return null;
		}
		}
	}
}
